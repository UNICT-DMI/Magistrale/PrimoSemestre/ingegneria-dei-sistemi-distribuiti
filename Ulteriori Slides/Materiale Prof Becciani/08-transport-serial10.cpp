/*
 *    It evolves the equation:
 *                            u,t + u,x + u,y = 0
 *    Using a Lax scheme.
 *    The initial data is a cruddy gaussian.
 *    Boundaries are flat: copying the value of the neighbour
Comando per il plot

gnuplot

splot 'transport.txt' w l lc rgb "violet", 'transport_end.txt' w l lc rgb "red"

 */

#include <cstdlib>
#include <fstream>
#include <string>
#include <sstream>
#include <math.h>  
#include <iostream>
using namespace std;

#define NX 100
#define NY 100
#define LX 2.0
#define LY 2.0 
//#define step 1200

/* 
 * conversions from discrete to real coordinates: can be better 
 */
float ix2x(int ix){
    return ix*LX/NX-LX/2;
}

float iy2y(int iy){ 
    return iy*LY/NY-LY/2;
}


/* 
 * initialize the system with a gaussian temperature distribution 
 */

int init_transport(double *temp){
    
    float sigma=0.1;
    int tmax=100;

    int ix,iy;
    float x,y;
    

    for(iy=1;iy<=NY;++iy)
	for(ix=1;ix<=NX;++ix){
	    x=ix2x(ix);
	    y=iy2y(iy);
	    temp[((NX+2)*iy)+ix] = tmax*exp((-((x*x)+(y*y)))/(2.0*(sigma*sigma)));
	}
    return 1;
}

/*
 * save the temperature distribution
 * the ascii format is suitable for splot gnuplot function
 */

int save_gnuplot(string filename, double *temp){
    
    int ix,iy;
    ofstream fp(filename.c_str());

    fp<<"\tX"<<"\tY"<<"\tu(t)"<<endl;  

    for(iy=1;iy<=NY;++iy)		
	for(ix=1;ix<=NX;++ix)
	    fp<<"\t "<<ix<<"\t "<<iy<<"\t "<<temp[((NX+2)*iy)+ix]<<endl;
    
    fp.close();

    return 1;
}

int update_boundaries_FLAT(double *temp){
    
    int ix=0, iy=0;

    for(iy=0;iy<=NY+1;++iy){
	temp[(NX+2)*iy] = temp[((NX+2)*iy)+1];
	temp[((NX+2)*iy)+(NX+1)] = temp[((NX+2)*iy)+NX];
    }
    
    for(ix=0;ix<=NX+1;++ix){
	temp[ix] = temp[(NX+2)+ix];
	temp[((NX+2)*(NY+1))+ix] = temp[((NX+2)*NY)+ix];
    }
    return 1;
}
int update_boundaries_PERIOD(double *temp)
{
    
    int ix=0, iy=0;

    for(iy=1;iy<=NX;++iy){
	temp[(NX+2)*iy] = temp[((NX+2)*iy)+NX]; //temp[102]=temp[202] ...
	temp[((NX+2)*iy)+(NX+1)] = temp[((NX+2)*iy)+1]; //temp[203]=temp[103]
    }
    
    for(ix=1;ix<=NX;++ix){
	temp[ix] = temp[(NX+2)*NY+ix];//temp[1]=temp[10201]
	temp[((NX+2)*(NY+1))+ix] =temp[(NX+2)+ix];//temp[10303]=temp[103]
    }
    temp[0]=temp[(NX+2)*(NY+1)-2]; //temp[0]=temp[10300]
    temp[NX+1]=temp[(NX+2)*NY+1]; //temp[101]=temp[10201]
    temp[(NX+2)*(NY+1)+1]=temp[(NX+2)*2-2]; //temp[10302]=temp[202]
    temp[(NX+2)*(NY+2)-1]=temp[(NX+2)+1]; //temp[10403]=temp[103]

    return 1;
}


int evolve(float dtfact, double *temp, double *temp_new, bool flat){
    
    float dx, dt;
    int ix, iy;
    double temp0;
    dx = LX/NX;
    dt = dtfact*dx/sqrt(3.0);
    for(iy=1;iy<=NY;++iy)
	for(ix=1;ix<=NX;++ix){
	    temp0 = temp[((NX+2)*iy)+ix];
	    temp_new[((NX+2)*iy)+ix] = temp0-0.5*dt*(temp[((NX+2)*iy)+(ix+1)]-temp[((NX+2)*iy)+(ix-1)]+temp[((NX+2)*(iy+1))+ix]-temp[((NX+2)*(iy-1))+ix])/(2*dx);
	}

    for(iy=0;iy<=NY+1;++iy)
	for(ix=0;ix<=NX+1;++ix)
	    temp[((NX+2)*iy)+ix] = temp_new[((NX+2)*iy)+ix];
    
    if(flat) 
	update_boundaries_FLAT(temp);
    else
    	update_boundaries_PERIOD(temp);

    return 1;
}


int main(int argc, char* argv[]){

    int i=0, nRow=NX+2, nCol=NY+2;
    double *temp, *temp_new;
    int step=0;
    bool flat=true;
    char flatchar[10], dtfactchar[10];
 
    cout<<"Flat or Periodic boundary condition (F/P) [F] ? ";
    cin.getline(flatchar,10);
    string flatstr(flatchar);

    if(flatstr.find("P") ==0 || flatstr.find("p") ==0)
    		flat=false;
    cout<<endl;
    if(flat) cout<<"Flat boundary condition applied"<<endl;
    else  cout<<"Periodic boundary condition applied"<<endl;

    temp = (double *) malloc (nRow*nCol*sizeof(double));
    temp_new = (double *) malloc (nRow*nCol*sizeof(double));
    cout<<"Number of steps of the evolution [1200] ? ";
    cin.getline(flatchar,10);
    stringstream stepsstr(flatchar);
    stepsstr>>step;
    if(step <=0) step=1200;    
    cout<<"Number of sevolution steps= "<<step<<endl;
    cout<<"Stability Factor [0.1] ? ";
    cin.getline(dtfactchar,10);
    stringstream stabsstr(dtfactchar);
    float dtfact=0;
    stabsstr>>dtfact;
    if(dtfact <=0) dtfact=0.1;    
    cout<<"Stability Factor= "<<dtfact<<endl;

    init_transport(temp);

    if(flat) 
	update_boundaries_FLAT(temp);
    else
    	update_boundaries_PERIOD(temp);

    string initialFilename("transport.txt");
    string finalFilename("transport_end.txt");
    save_gnuplot(initialFilename, temp);

    for(i=1;i<=step;++i) evolve(dtfact, temp, temp_new,flat);

    save_gnuplot(finalFilename, temp);

    return 1;
}
