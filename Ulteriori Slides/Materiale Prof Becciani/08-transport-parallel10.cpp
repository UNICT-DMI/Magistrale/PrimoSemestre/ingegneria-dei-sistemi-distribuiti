/*
 *    It evolves the equation:
 *                            u,t + u,x + u,y = 0
 *    Using a FTCS scheme.
 *    The initial data is a cruddy gaussian.
 *    Boundaries are flat: copying the value of the neighbour, or periodic

Comando per il plot

gnuplot

splot 'transport.txt' w l lc rgb "violet", 'transport_end.txt' w l lc rgb "red"

 */
 
#include <mpi.h>
#include <cstdlib>
#include <fstream>
#include <string>
#include <sstream>
#include <math.h>  
#include <iostream>
using namespace std;
const  float LX= 2.0;
const  float LY= 2.0;
const  int NX=100;
const  int NY=100;

/* conversions from discrete to real coordinates: can be better */
float ix2x(int ix){
    return ix*LX/NX-LX/2; 
}


/*
 * THIS CHANGES: the every processor has a different offset
 * I pass also the proc_y and I do not use proc_me directy because
 * saving the file I use this parameter to evaluate the real y coordinate
 * for all the data of other processors that are saved by 0 however.
 */

float iy2y(int iy, int NLY, int proc_y){ 
    return iy*LY/NY-LY/2 + proc_y*NLY*LY/NY;  
}

/* initialize the system with a gaussian temperature distribution */


int init_transport(double *temp, int NLY,  int proc_me)
{
    float sigma=0.1;
    int tmax=100;
    int count=0;
    int ix,iy;
    float x,y;
//     ofstream fp;
//     if(proc_me==0) fp.open("0.txt");
//     if(proc_me==1) fp.open("1.txt");
//     if(proc_me==2) fp.open("2.txt");
//     if(proc_me==3) fp.open("3.txt");
//       	fp<<"\tX"<<"\tY"<<"\tu(t)"<<endl;  

    for(iy=1;iy<=NLY;++iy)
    {
	count++;
	for(ix=1;ix<=NX;++ix){
	    x=ix2x(ix);
	    y=iy2y(iy,NLY,proc_me);
	    temp[((NX+2)*iy)+ix] = tmax*exp((-((x*x)+(y*y)))/(2.0*(sigma*sigma)));
// 	    fp<<"\t "<<ix<<"\t "<<count<<"\t "<<temp[((NX+2)*iy)+ix]<<" x="<<x<<" y="<<y<<endl;

	}
    }
/*    fp.close();*/
    return 1;
}

/*
 * save the temperature distribution
 * the ascii format is suitable for splot gnuplot function
 */

int save_gnuplot(string filename, double *temp, double *temp_new, int proc_me, int nprocs){

    int ix, iy, iproc=0,  remoteRows, rowNumber=0;
 
    MPI_Status status;
    remoteRows=NY/nprocs;
    if(proc_me+1 <= NY%nprocs) remoteRows++;  

    if(proc_me == 0){
  	ofstream fp(filename.c_str());
      	fp<<"\tX"<<"\tY"<<"\tu(t)"<<endl;  
	for(iy=1;iy<=remoteRows;++iy){		
		rowNumber++;
	    for(ix=1;ix<=NX;++ix){
		fp<<"\t "<<ix<<"\t "<<rowNumber<<"\t "<<temp[((NX+2)*iy)+ix]<<endl;
	    }

	}

	for(iproc=1;iproc<nprocs;iproc++){
	    remoteRows=NY/nprocs;
            if(iproc+1 <= NY%nprocs) remoteRows++;  
	    MPI_Recv(temp_new, (NX+2)*(remoteRows+2), MPI_DOUBLE, iproc, 0, MPI_COMM_WORLD, &status);
	    for(iy=1;iy<=remoteRows;++iy){		
		rowNumber++;
		for(ix=1;ix<=NX;++ix){
		fp<<"\t "<<ix<<"\t "<<rowNumber<<"\t "<<temp_new[((NX+2)*iy)+ix]<<endl;
		}
	    }
	}
	fp.close();
    }
    else{
	MPI_Send(temp, (NX+2)*(remoteRows+2), MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }

    return 1;
}

int update_boundaries_FLAT(double *temp, int NLY, int nprocs, int proc_me, int proc_down, int proc_up){

    MPI_Status status, status1;
    int iy=0, ix=0;
    
    for(iy=1;iy<=NLY;++iy){
	temp[(NX+2)*iy] = temp[((NX+2)*iy)+1];
	temp[((NX+2)*iy)+(NX+1)] = temp[((NX+2)*iy)+NX];
    }

/*  only the lowest has the lower boundary condition  */
    if (proc_me==0) for(ix=0;ix<=NX+1;++ix) temp[ix] = temp[(NX+2)+ix];
    
/*  only the highest has the upper boundary condition  */
    if (proc_me==nprocs-1) for(ix=0;ix<=NX+1;++ix) temp[((NX+2)*(NLY+1))+ix] = temp[((NX+2)*(NLY))+ix];

/*  communicate the ghost-cells  */
/*  lower-down  */
    MPI_Sendrecv(&temp[(NX+2)], NX+2, MPI_DOUBLE, proc_up, 0, &temp[((NX+2)*(NLY+1))], NX+2, MPI_DOUBLE, proc_down, 0, MPI_COMM_WORLD, &status);


/*  higher-up  */
    MPI_Sendrecv(&temp[((NX+2)*(NLY))], NX+2, MPI_DOUBLE, proc_down, 0, &temp[0], NX+2, MPI_DOUBLE, proc_up, 0, MPI_COMM_WORLD, &status1);

    return 1;
}
int update_boundaries_PERIOD(double *temp, int NLY, int nprocs, int proc_me, int proc_down, int proc_up)
{
    MPI_Status status, status1;
    
    int ix=0, iy=0;


    for(iy=1;iy<=NLY;++iy){
	temp[(NX+2)*iy] = temp[((NX+2)*iy)+NX];
	temp[((NX+2)*iy)+(NX+1)] = temp[((NX+2)*iy)+1];
    }

/*  communicate the ghost-cells  */
/*  lower-down  */

//     stringstream tmp,tmp1;
//     tmp<<"PE= "<<proc_me<<" Invio temp["<<NX+2<<"] a PE="<<proc_up<<endl;
//     tmp<<" Ricevo temp["<<(NX+2)*(NLY+1)<<"] da PE="<<proc_down<<endl;

    MPI_Sendrecv(&temp[(NX+2)], NX+2, MPI_DOUBLE, proc_up, 0, &temp[((NX+2)*(NLY+1))], NX+2, MPI_DOUBLE, proc_down, 0, MPI_COMM_WORLD, &status);

//    cout<<tmp.str();

/*  higher-up  */
    MPI_Sendrecv(&temp[((NX+2)*(NLY))], NX+2, MPI_DOUBLE, proc_down, 0, &temp[0], NX+2, MPI_DOUBLE, proc_up, 0, MPI_COMM_WORLD, &status1);

//     tmp1<<"PE= "<<proc_me<<" Invio temp["<<((NX+2)*(NLY))<<"] a PE="<<proc_down<<endl;
//     tmp1<<" Ricevo temp["<<0<<"] da PE="<<proc_up<<endl;    
//     cout<<tmp1.str();


   if(proc_me==0)
   {
    temp[0]=temp[NX]; //temp[0](=temp[10300])==temp[100]
    temp[NX+1]=temp[1]; //temp[101](=temp[10201])==temp[1]
  }
   if(proc_me==nprocs-1)
   {
    temp[(NX+2)*(NLY+1)]=temp[(NX+2)*(NLY+1)+NX]; //(temp[10302]=temp[202]) temp[2652]=temp[2752]
    temp[(NX+2)*(NLY+1)+NX+1]=temp[(NX+2)*(NLY+1)+1]; //(temp[10403]=temp[103]) temp[2753]=temp[2653]
  }







    return 1;
}


int evolve(float dtfact, double *temp, double *temp_new,  int NLY,int flat){
    
    float dx, dt;
    int ix, iy;
    double temp0;
    
    dx = 2*LX/NX;
    dt = dtfact*dx/sqrt(3.0);
    
    for(iy=1;iy<=NLY;++iy)
	for(ix=1;ix<=NX;++ix){
	    temp0 = temp[((NX+2)*iy)+ix];
	    temp_new[((NX+2)*iy)+ix] = temp0-0.5*dt*(temp[((NX+2)*(iy+1))+ix]-temp[((NX+2)*(iy-1))+ix]+temp[((NX+2)*iy)+(ix+1)]-temp[((NX+2)*iy)+(ix-1)])/dx;
	}
    
    for(iy=0;iy<NLY+2;++iy)
	for(ix=0;ix<NX+2;++ix)
	    temp[((NX+2)*iy)+ix] = temp_new[((NX+2)*iy)+ix];
    
    return 1;
}


int main(int argc, char* argv[]){

    int nprocs, proc_me, proc_down, proc_up;
    int i=0, NLY;
    double *temp, *temp_new;
    float dtfact=0;
    int step=0;
    int flat=1;
    char flatchar[10], dtfactchar[10];
 
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_me);

    if(proc_me==0)
    {
      cout<<"Flat or Periodic boundary condition (F/P) [F] ? ";
      cin.getline(flatchar,10);
      string flatstr(flatchar);

      if(flatstr.find("P") ==0 || flatstr.find("p") ==0)
    		flat=0;
      cout<<endl;
      if(flat) cout<<"Flat boundary condition applied"<<endl;
      else  cout<<"Periodic boundary condition applied"<<endl;

      cout<<"Number of steps of the evolution [600] ? ";
      cin.getline(flatchar,10);
      stringstream stepsstr(flatchar);
      stepsstr>>step;
      if(step <=0) step=600;    
      cout<<"Number of sevolution steps= "<<step<<endl;
      cout<<"Stability Factor [0.1] ? ";
      cin.getline(dtfactchar,10);
      stringstream stabsstr(dtfactchar);
      stabsstr>>dtfact;
      if(dtfact <=0) dtfact=0.1;    
      cout<<"Stability Factor= "<<dtfact<<endl;
    }
    MPI_Bcast(&flat,1, MPI_INT, 0, MPI_COMM_WORLD );
    MPI_Bcast(&step,1, MPI_INT, 0, MPI_COMM_WORLD );
    MPI_Bcast(&dtfact,1, MPI_FLOAT, 0, MPI_COMM_WORLD );



/*  
 *  all the communications from/to MPI_PROC_NULL do nothing
 */
    if(proc_me==0){
	if(flat==1) proc_up = MPI_PROC_NULL;
	else proc_up=nprocs-1;
    }else{
	proc_up = proc_me-1;
    }
    if(proc_me==nprocs-1){
	if(flat==1) proc_down = MPI_PROC_NULL;
	else proc_down=0;
    }else{
	proc_down = proc_me+1;
    }

    NLY = NY/nprocs;
    if(proc_me+1 <= NY%nprocs) NLY++;  // nel caso di non perfetta divisibilità....
    stringstream sstmp;
    sstmp<<"PE="<<proc_me<<" Local Rows= "<<NLY<<endl;
    cout<<sstmp.str()<<endl;
    sstmp.str(""); //setta a "vuota" la streangstream
    
    temp = (double *) malloc ((NX+2)*(NLY+2)*sizeof(double));
    temp_new = (double *) malloc ((NX+2)*(NLY+2)*sizeof(double));

    init_transport(temp, NLY, proc_me);
    if(flat) 
	update_boundaries_FLAT(temp, NLY, nprocs, proc_me, proc_down, proc_up);
    else
    	update_boundaries_PERIOD(temp, NLY, nprocs, proc_me, proc_down, proc_up);

    string initialFilename("transport.txt");
    string finalFilename("transport_end.txt");
    save_gnuplot(initialFilename, temp, temp_new, proc_me, nprocs);

    for(i=1;i<=step;++i){
	evolve(dtfact, temp, temp_new, NLY,flat);
	if(flat==1) 
		update_boundaries_FLAT(temp, NLY,  nprocs, proc_me, proc_down, proc_up);
	else 
		update_boundaries_PERIOD(temp, NLY,  nprocs, proc_me, proc_down, proc_up);

    }
    
    save_gnuplot(finalFilename,  temp, temp_new, proc_me, nprocs);   

    delete [] temp;
    delete [] temp_new;
    MPI_Finalize();
    return 1;
}
